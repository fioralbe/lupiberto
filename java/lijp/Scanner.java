//package com.craftinginterpreters.lox;
package lijp;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import static com.craftinginterpreters.lox.TokenType.*; 
import static lijp.TokenType.*; 

class Scanner {
  private final String source;
  private final List<Token> tokens = new ArrayList<>();
  private int start = 0;
  private int current = 0;
  private int line = 1;

  private static final Map<String, TokenType> keywords;

  static {
    keywords = new HashMap<>();
    keywords.put("app",    APP);
    keywords.put("@",    APP);
    keywords.put("λ",  LAMBDA);
    keywords.put("l",  LAMBDA);
  }

  Scanner(String source) {
    this.source = source;
  }
  List<Token> scanTokens() {
    while (!isAtEnd()) {
      // We are at the beginning of the next lexeme.
      start = current;
      scanToken();
    }

    tokens.add(new Token(EOF, "", null, line, start));
    return tokens;
  }

  private void scanToken() {
    char c = advance();
    switch (c) {
      case '(': addToken(LEFT_PAREN); break;
      case ')': addToken(RIGHT_PAREN); break;

      case ',': 
      case ' ':
      case '\r':
      case '\t':
        // Ignore whitespace and commas.
        break;

      case '\n':
        line++;
        break;


      // case '"': string(); break;

      default:
        if (isAtomChar(c)) {
          identifier();
        } else {
          Main.error(line, "Unexpected character.");
        }
        break;
    }
  }

  private void identifier() {
    while (!isAtEnd() && isAtomChar(peek())) advance();

    // See if the identifier is a reserved word.
    String text = source.substring(start, current);

    TokenType type = keywords.get(text);
    if (type == null) type = IDENTIFIER;
    addToken(type);
  }
  // private void number() {
  //   while (isDigit(peek())) advance();

  //   // Look for a fractional part.
  //   if (peek() == '.' && isDigit(peekNext())) {
  //     // Consume the "."
  //     advance();

  //     while (isDigit(peek())) advance();
  //   }

  //   addToken(NUMBER,
  //       Double.parseDouble(source.substring(start, current)));
  // }
  // private void string() {
  //   while (peek() != '"' && !isAtEnd()) {
  //     if (peek() == '\n') line++;
  //     advance();
  //   }

  //   // Unterminated string.
  //   if (isAtEnd()) {
  //     Main.error(line, "Unterminated string.");
  //     return;
  //   }

  //   // The closing ".
  //   advance();

  //   // Trim the surrounding quotes.
  //   String value = source.substring(start + 1, current - 1);
  //   addToken(STRING, value);
  // }
  // private boolean match(char expected) {
  //   if (isAtEnd()) return false;
  //   if (source.charAt(current) != expected) return false;

  //   current++;
  //   return true;
  // }
  private char peek() {
    // if peek is called when isAtEnd is true this goes poof
    // if (isAtEnd()) return '\0';
    return source.charAt(current);
  }
  // private char peekNext() {
  //   if (current + 1 >= source.length()) return '\0';
  //   return source.charAt(current + 1);
  // }
  // private boolean isAlpha(char c) {
  //   return (c >= 'a' && c <= 'z') ||
  //          (c >= 'A' && c <= 'Z') ||
  //           c == '_';
  // }
  private boolean isAtomChar(char c) {
    return (
      "0123456789" + 
      "abcdefghijklmopqrstuvwxyz" +
      "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + 
      "λ" +
      "@-_."
      ).contains(Character.toString(c));
  }
  // private boolean isAlphaNumeric(char c) {
  //   return isAlpha(c) || isDigit(c);
  // }
  // private boolean isDigit(char c) {
  //   return c >= '0' && c <= '9';
  // } 


  private boolean isAtEnd() {
    // true iff current is not a valid index
    return current >= source.length();
  }
  private char advance() {
    current++;
    return source.charAt(current - 1);
  }

  private void addToken(TokenType type) {
    addToken(type, null);
  }

  private void addToken(TokenType type, Object literal) {
    String text = source.substring(start, current);
    tokens.add(new Token(type, text, literal, line, start));
  }
}


